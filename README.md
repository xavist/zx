# ZX - Small utils lib for ZSH scripts

Just some common functions I'm using in my ZSH testing and dotfiles scripts

Files are meant to be sourced, so I'm using the 'zshs' extension.

To run the tests you'll need [zshunit](https://bitbucket.org/xavist/zshunit)
